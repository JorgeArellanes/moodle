<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('curp');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('role');
		echo $this->Form->input('nombre');
		echo $this->Form->input('apellido_paterno');
		echo $this->Form->input('apellido_materno');
		echo $this->Form->input('email');
		echo $this->Form->input('fecha_nacimiento');
		echo $this->Form->input('celular');
		echo $this->Form->input('compania_telefonica');
		echo $this->Form->input('modifield');
		echo $this->Form->input('genero');
		echo $this->Form->input('foto_perfil');
		echo $this->Form->input('status');
		echo $this->Form->input('last_login');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Administrativos'), array('controller' => 'administrativos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Administrativo'), array('controller' => 'administrativos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Alumnos'), array('controller' => 'alumnos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Alumno'), array('controller' => 'alumnos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Profesores'), array('controller' => 'profesores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Profesore'), array('controller' => 'profesores', 'action' => 'add')); ?> </li>
	</ul>
</div>
