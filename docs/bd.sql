/* 
 * Base de datos de la Plataforma Educativa Inncomex.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Jorge Carreño Arellanes For Inncomex
 * Created: 26/11/2015
 */

SET foreign_key_checks = 0;

-- roles --
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
	`id` INT(11) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
	`user_id` INT (11) NOT NULL,
	`role` VARCHAR (15) NOT NULL,
	`created` DATETIME NOT NULL,
        CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- End SQL-Dump --
-- administrativos --
DROP TABLE IF EXISTS `administrativos`;
CREATE TABLE IF NOT EXISTS `administrativos` (
	`id` INT(11) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
	`cargo` VARCHAR(100) DEFAULT NULL,
	`user_id` INT (11) NOT NULL UNIQUE,
        CONSTRAINT `administrativos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- End SQL-Dump --
-- profesores --
DROP TABLE IF EXISTS `profesores`;
CREATE TABLE IF NOT EXISTS `profesores` (
	`id` INT(11) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
	`user_id` INT (11) NOT NULL UNIQUE,
        `domicilio` VARCHAR (100),
        CONSTRAINT `profesores_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- End SQL-Dump --
-- alumnos --
DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE IF NOT EXISTS `alumnos` (
	`id` INT(11) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
	`user_id` INT (11) NOT NULL UNIQUE,
	`promedio_general` FLOAT DEFAULT 0 NOT NULL,
        CONSTRAINT `alumnos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- End SQL-Dump --
-- users --
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
	`id` INT (11) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,
        `curp` VARCHAR(18) NOT NULL UNIQUE,
	`username` VARCHAR (30) NOT NULL UNIQUE,
	`password` VARCHAR (100) NOT NULL,
	`role` VARCHAR(15) DEFAULT "ALUMNO" NOT NULL,
	`nombre` VARCHAR (25) NOT NULL,
	`apellido_paterno` VARCHAR (25) DEFAULT NULL,
	`apellido_materno` VARCHAR (25) DEFAULT NULL,
	`email` VARCHAR (100) NOT NULL UNIQUE,
	`fecha_nacimiento` DATE NOT NULL,
	`celular` VARCHAR(15) DEFAULT NULL,
	`compania_telefonica` VARCHAR (15) DEFAULT NULL,
	`created` DATETIME NOT NULL,
	`modifield` DATETIME NOT NULL,
	`genero` CHAR NOT NULL,
	`foto_perfil` VARCHAR (100),
	`status` INT (1),
	`last_login` DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- End SQL-Dump --